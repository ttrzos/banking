import pickle


def pickle_load(path: str):
    with open(path, 'rb') as file:
        return pickle.load(file)


def pickle_save(obj: object, path: str):
    with open(path, 'wb') as handle:
        pickle.dump(obj, handle)


def read_text_file(path: str):
    with open(path, 'rb') as file:
        return file.read()


def save_text_file(text: str, path: str):
    with open(path, 'w') as file:
        file.write(text)


def read_text_and_eval(path: str):
    return eval(read_text_file(path))


def print_contractors_dictionary(cd: dict):
    for k, v in {k: max(v, key=v.get) for k, v in cd.items()}.items():
        print(k, " : ", v)

