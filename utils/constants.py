from pathlib import Path

DATA_FOLDER = Path('C:/code/projects/banking/data')
RAW_DATA_FOLDER = DATA_FOLDER / 'raw'
PREPROCESSED_DATA_FOLDER = DATA_FOLDER / 'preprocessed'
VISUALIZATIONS_FOLDER = DATA_FOLDER / 'visualizations'
CATEGORIES = [
    'Groceries', 'Food/Restaurant', 'Transportation', 'Shopping', 'Entertainment', 'Healthcare', 'Subscription',
    'Miscellaneous'
]
EXAMPLE_CATEGORIES = {
    'CIRCLE K': 'Transportation',
    'DINO': 'Groceries',
    'SHELL': 'Transportation',
    'APTEKA HYGIEIA': 'Healthcare',
    'ORLEN STACJA': 'Transportation',
    'Glovo': 'Food/Restaurant',
    'Allegro': 'Shopping',
    'MALINOWY ANIOL': 'Food/Restaurant',
    'BP-REKAWKA': 'Transportation',
    'BP-ADAS': 'Transportation',
    'Multikino': 'Entertainment',
    'BIEDRONKA': 'Groceries',
    'IKEA': 'Shopping',
    'FRESH BARBER': 'Miscellaneous',
    'Kaska Sis': 'Miscellaneous',
    'Pierwszy Urząd Skarbowy Opole': 'Miscellaneous',
    'Natalia': 'Miscellaneous',
}
