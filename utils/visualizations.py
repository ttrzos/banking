import warnings

import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

from .constants import VISUALIZATIONS_FOLDER

matplotlib.use('Agg')
sns.set_style("darkgrid")

warnings.filterwarnings('ignore')


def save_figure(fig, title, folder, extension: str = '.png'):
    save_path = folder / f"{title}.{extension}"


def pie_bar_chart(data, x_column, y_column, title, folder=VISUALIZATIONS_FOLDER, palette='plasma', prefix: str='',
                  currency: str = 'zł'):
    # Set up the figure and axis objects
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 7))

    # Create a barplot on the left (ax1)
    bar_data = data[[x_column, y_column]]
    bar_data = bar_data.groupby(x_column).agg({y_column: 'sum'}).rename(columns={y_column: f'total expenses [{currency}]'}).sort_index()
    barplot = sns.barplot(data=bar_data.reset_index(), x=x_column, y=f'total expenses [{currency}]', ax=ax1, palette=palette)
    barplot.set_xticklabels(barplot.get_xticklabels(), rotation=45)
    barplot.set_title(f"{prefix+' '}Exepnses: {bar_data[f'total expenses [{currency}]'].sum():9.2f} {currency}", loc='left')

    for container in ax1.containers:
        ax1.bar_label(container, fmt='%d', color='blue')

    # Prepare data for the pie chart
    pie_data = data.groupby(x_column)[y_column].sum().reset_index()

    colors = sns.color_palette(palette, len(pie_data))

    # Create a pie chart on the right (ax2)
    wedges, texts, autotexts = ax2.pie(pie_data[y_column], labels=pie_data[x_column], autopct='%1.1f%%', startangle=140,
                                       colors=colors)

    # Change the color of the percentages in the pie chart to light blue
    for autotext in autotexts:
        autotext.set_color('white')

    # Add a global title
    plt.suptitle(title, fontsize=16)

    # Adjust the layout
    plt.tight_layout(rect=(0, 0, 1, 0.90))

    # Save the figure
    plt.savefig(folder / f"PieBar_{title}.png")
