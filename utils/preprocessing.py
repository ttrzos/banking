import os
from typing import List, Dict, Optional
from pathlib import Path

import numpy as np
import pandas as pd
from openai import OpenAI

from .constants import *

pd.options.display.width = 1000
pd.options.display.max_columns = 20
pd.options.display.max_colwidth = 100


class Preprocessor:

    def __init__(
            self,
            categories: List[str],
            api_key: str | None = None,
    ):
        self.API_KEY = api_key
        self.categories = categories if categories is not None else ['Groceries', 'Food/Restaurant', 'Transportation',
                                                                     'Shopping', 'Entertainment', 'Healthcare',
                                                                     'Subscription', 'Miscellaneous']

    @staticmethod
    def preprocess_raw_csv(file_name: str) -> pd.DataFrame:
        """
        Function for preprocessing raw transaction file from bank into a valid csv file. Must be in DATA_FOLDER.

        :param file_name:
        :return:
        """

        assert Path(file_name) in RAW_DATA_FOLDER.iterdir(), "File not present in RAW_DATA_FOLDER!"

        f = open(RAW_DATA_FOLDER / file_name, "r")
        content = f.read()

        content = content.replace('³', 'ł').replace('œ', 'ś').replace('æ', 'ć') \
            .replace('¹', 'ą').replace('ê', 'ę').replace('"', "") \
            .replace('Ê', 'Ę')
        content = "\n".join([";".join(line.split(';')[:16]) for line in content.split("\n")][18:-4])

        # with open(PREPROCESSED_DATA_FOLDER / file_name, 'w', encoding='utf-8') as f:
        #     f.write(content)

        lines = [x.split(';') for x in content.split('\n')]
        columns, lines = lines[0], lines[1:]
        columns = [f'{ele}_{columns[:idx].count(ele)}' if (ele in columns[:idx]) else ele for idx, ele in enumerate(columns)]

        df = pd.DataFrame(lines, columns=columns)

        return df.replace('', np.nan)

    @staticmethod
    def clean_preprocessed_csv(df: pd.DataFrame) -> pd.DataFrame:

        df = df.rename(columns={
            'Data transakcji': 'transaction_date',
            'Data księgowania': 'posting_date',
            'Dane kontrahenta': 'contractor_details',
            'Tytuł': 'title',
            'Nr rachunku': 'bill_number',
            'Nazwa banku': 'bank_name',
            'Szczegóły': 'details',
            'Nr transakcji': 'transaction_number',
            'Kwota transakcji (waluta rachunku)': 'transaction_amount',
            'Waluta': 'currency',
            'Kwota płatności w walucie': 'transaction_amount_currency',
            'Waluta_1': 'currency_1',
            'Waluta_2': 'currency_2',
            'Saldo po transakcji': 'balance_after_transaction'
        })

        df['transaction_amount'] = df['transaction_amount'].fillna(
            df['Kwota blokady/zwolnienie blokady']
        )
        df = df.drop(columns=['Kwota blokady/zwolnienie blokady'])
        df['transaction_amount'] = df['transaction_amount'].apply(lambda x: x.replace(',', '.')).astype(float)

        df = df.loc[df.transaction_amount <= 0]
        df['transaction_amount'] = df['transaction_amount'] * -1
        df['transaction_date'] = pd.to_datetime(df['transaction_date'])
        df['posting_date'] = pd.to_datetime(df['posting_date'])
        df['year_month'] = df.transaction_date.dt.to_period('M')

        df = df.loc[~df.contractor_details.str.contains('TRZOS TOMASZ')]

        for col in ['contractor_details', 'title', 'bill_number', 'bank_name', 'details', 'transaction_number']:
            df[col] = df[col].str.strip(' ')

        return df.reset_index(drop=True)

    def categorize_contractors(self, contractor_details: list | str, examples: dict={}):
        """

        :param contractor_details:
        :param examples:
        :return:
        """

        if isinstance(contractor_details, list):
            contractor_details = str(contractor_details).replace(']', '').replace('[', '')

        client = OpenAI(api_key=self.API_KEY)

        prompt = f"""
        You will be provided with a list of contractor details in a list like format. 
        Your task is to categorize provided contractor details into one of these categories:
            {self.categories}
        Select categories carefully, considering every single one and calculate probabilities for every category after a
         in-depth analysis. Focus on understanding the categories and match the categories as good as possible, 
         assigning higher probabilities to categories of better fit.
        Return ONLY a python dictionary, with keys being the provided contractor details, and values as
        another dictionary with categories as keys and their probabilities as values.
        
        """

        if examples:
            prompt += "\nWhen answering user questions follow these examples:" + \
                  "\n".join(f"{k}: {v}" for k, v in examples.items()) + "\n"

        user_prompt = f"""
            Content to categorize (contractor details): 
            {contractor_details}
        Output probabilities:
        """

        chat_completion = client.chat.completions.create(
            messages=[
                {
                    "role": "system",
                    "content": prompt
                },
                {
                    "role": "user",
                    "content": user_prompt
                }
            ],
            model="gpt-3.5-turbo",
        )

        categories = chat_completion.choices[0].message.content.strip()
        categories_dict = eval(categories)

        return categories_dict

    @staticmethod
    def extract_best_categories(categories_dictionary: dict[str, dict[str, float]]):
        """

        """
        return {key: max(value, key=value.get) for key, value in categories_dictionary.items()}

    def process_files(
            self,
            file_names: list,
            categories_dictionary: Optional[dict[str, str]] = {},
            examples: dict={},
            batch_size: int = 15,
    ) -> (pd.DataFrame, dict):
        """
        Process a list of CSV files by preprocessing, cleaning and categorizing them.

        Args:
            file_names (list): List of file names to be processed.
            categories_dictionary (dict): Dictionary with predicted contractor details categories.
            batch_size (int): Number of elements present in a single OpenAI request.

        Returns:
            pd.DataFrame: Concatenated DataFrame of all processed files.
        """

        dfs = [self.clean_preprocessed_csv(self.preprocess_raw_csv(RAW_DATA_FOLDER / file)) for file in file_names]

        df = pd.concat(dfs)
        to_categorize = df.contractor_details.unique().tolist()

        if categories_dictionary:
            to_categorize = [x for x in to_categorize if x not in categories_dictionary.keys()]

        # TODO: batch implementation
        n_batches = int(np.ceil(len(to_categorize) / batch_size))

        print(f"Batch requests to send: {n_batches}")
        if to_categorize:
            print(f"Requests sent: ", end='')

            for i, batch in enumerate(np.array_split(to_categorize, n_batches)):
                print(i+1, end=", ")

                while True:
                    try:
                        categories_dict = self.categorize_contractors(batch.tolist(), examples=examples)
                        categories_dictionary.update(categories_dict)
                        break
                    except SyntaxError:
                        print(i + 1, end=", ")
                        pass
            # TODO: check if every contractor got categorized
            print()

        categories = self.extract_best_categories(categories_dictionary)
        df['category'] = df.contractor_details.map(categories)

        return df, categories_dictionary
