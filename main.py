import os
from dotenv import load_dotenv

import pandas as pd

from utils import *

load_dotenv()


def main():

    OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
    file_names_path = PREPROCESSED_DATA_FOLDER / 'preprocessed_files.txt'
    dict_path = PREPROCESSED_DATA_FOLDER / 'contractors_dictionary.pickle'

    df_trans = pd.read_csv(PREPROCESSED_DATA_FOLDER / 'Transactions_preprocessed.csv',
                           parse_dates=['posting_date', 'transaction_date'])
    file_names = read_text_and_eval(file_names_path)
    contractors_dict = pickle_load(dict_path)

    # Preprocess new transaction files
    new_file_names = [x for x in os.listdir(RAW_DATA_FOLDER) if x not in file_names]

    if new_file_names:
        print(f"Files to preprocess: {new_file_names}")

        prepro = Preprocessor(categories=CATEGORIES, api_key=OPENAI_API_KEY)
        df, categories_dictionary = prepro.process_files(
            file_names=new_file_names, categories_dictionary=contractors_dict, examples=EXAMPLE_CATEGORIES
        )

        # Save  data file names and contractors dictionary
        save_text_file(str(file_names + new_file_names), file_names_path)
        pickle_save(categories_dictionary, dict_path)

        # Create summary plots
        for year_month, g in df.groupby('year_month'):

            _ = pie_bar_chart(
                g, 'category', 'transaction_amount',
                title=f'Transaction_summary_{year_month}',
                palette="viridis"
            )

            # TODO: more visualizations

        # DF Concat and save
        df_trans = pd.concat([df_trans, df])
        df_trans.to_csv(PREPROCESSED_DATA_FOLDER / "Transactions_preprocessed.csv", index=False)

    else:
        print("No files to preprocess")

    # Total plot
    _ = pie_bar_chart(
        df_trans, 'category', 'transaction_amount',
        title=f'Transaction_summary_total',
        palette="viridis",
        prefix='Total'
    )

    # Monthly average plot
    _ = pie_bar_chart(
        df_trans.groupby(['year_month', 'category']).agg({'transaction_amount': 'sum'}).reset_index() \
            .groupby(['category']).agg({'transaction_amount': 'mean'}).reset_index(),
        'category', 'transaction_amount',
        title=f'Transaction_summary_average',
        palette="viridis",
        prefix='Monthly Average'
    )


if __name__ == '__main__':
    main()
